package org.ws4d.wscompactsecurity.authentication.crypto;

import java.security.KeyPair;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;

public class AuthenticatedECCDHParameters {
	
	String mOrigin = null;
	String mTarget = null;
	
	byte[] mNonceA;
	byte[] mNonceB;
	
	String mCurveName = null;
	KeyPair mKeyPair = null;
	
	PublicKey mForeignPublicKey = null;
	PublicKey mDistortedPublicKey = null;
	
	boolean[] mOOBSharedSecret = null;
	int mOOBSharedSecretAsInt;
	byte[] mSessionKey = null;
	byte[] mMasterKey = null;
	byte[] mCMAC = null;
	byte[] mForeignCMAC = null;
	
	public byte[] getForeignCMAC() {
		return mForeignCMAC;
	}

	public void setForeignCMAC(byte[] foreignCMAC) {
		this.mForeignCMAC = foreignCMAC;
	}

	HashSet<String> mOtherA = null;
	HashSet<String> mOtherB = null;
	
	public HashSet<String> getOtherA() {
		return mOtherA;
	}

	public void setOtherA(HashSet<String>  otherA) {
		this.mOtherA = otherA;
	}

	public HashSet<String> getOtherB() {
		return mOtherB;
	}

	public void setOtherB(HashSet<String> otherB) {
		this.mOtherB = otherB;
	}
	
	public void addOtherA(String s) {
		this.mOtherA.add(s);
	}

	public void addOtherB(String s) {
		this.mOtherB.add(s);
	}
	
	public String otherAasString() {
		String result = "";
		
		/* HashSets do not allow duplicates (good) but don't allow sorting either (bad)
		 * However, ArrayList allows sorting (good) but also allows duplicated (bad) */
		ArrayList<String> sorted = new ArrayList<String>();
		
		sorted.addAll(mOtherA);
		
		Collections.sort(sorted);
		
		for (String s : sorted) {
			result += s;
		}
		
		return result;
	}

	public String otherBasString() {
		String result = "";
		
		ArrayList<String> sorted = new ArrayList<String>();
		
		sorted.addAll(mOtherB);
		
		Collections.sort(sorted);
		
		for (String s : sorted) {
			result += s;
		}
		
		return result;
	}

	boolean mIsFirst = false; /* whether or not this participant is the first one
							  * to transmit its public key, which means it gets
							  * scrambled
							  * ************************************************ */
	
	public AuthenticatedECCDHParameters() {
		mOtherA = new HashSet<String>();
		mOtherB = new HashSet<String>();
	}
	
	public boolean getIsFirst() {
		return mIsFirst;
	}
	
	public void setIsFirst(boolean isFirst) {
		this.mIsFirst = isFirst;
	}

	public String getOrigin() {
		return mOrigin;
	}

	public void setOrigin(String origin) {
		this.mOrigin = origin;
	}

	public String getTarget() {
		return mTarget;
	}

	public void setTarget(String target) {
		this.mTarget = target;
	}

	public byte[] getNonceA() {
		return mNonceA;
	}

	public void setNonceA(byte[] nonce) {
		this.mNonceA = nonce;
	}

	public byte[] getNonceB() {
		return mNonceB;
	}

	public void setNonceB(byte[] nonce) {
		this.mNonceB = nonce;
	}

	public String getCurveName() {
		return mCurveName;
	}

	public void setCurveName(String curveName) {
		this.mCurveName = curveName;
	}

	public KeyPair getKeyPair() {
		return mKeyPair;
	}

	public void setKeyPair(KeyPair keyPair) {
		this.mKeyPair = keyPair;
	}

	public PublicKey getForeignPublicKey() {
		return mForeignPublicKey;
	}

	public void setForeignPublicKey(PublicKey foreignPublicKey) {
		this.mForeignPublicKey = foreignPublicKey;
	}

	public PublicKey getDistortedPublicKey() {
		return mDistortedPublicKey;
	}

	public void setDistortedPublicKey(PublicKey distortedPublicKey) {
		this.mDistortedPublicKey = distortedPublicKey;
	}

	public byte[] getCMAC() {
		return mCMAC;
	}

	public void setCMAC(byte[] cMAC) {
		mCMAC = cMAC;
	}

	public boolean[] getOOBSharedSecret() {
		return mOOBSharedSecret;
	}

	public void setOOBSharedSecret(boolean[] oobSharedSecret) {
		this.mOOBSharedSecret = oobSharedSecret;
	}

	public int getOOBSharedSecretAsInt() {
		return mOOBSharedSecretAsInt;
	}

	public void setOOBSharedSecretAsInt(int oobSharedSecret) {
		this.mOOBSharedSecretAsInt = oobSharedSecret;
	}

	public byte[] getSessionKey() {
		return mSessionKey;
	}

	public void setSessionKey(byte[] sessionKey) {
		this.mSessionKey = sessionKey;
	}

	public byte[] getMasterKey() {
		return mMasterKey;
	}

	public void setMasterKey(byte[] masterKey) {
		this.mMasterKey = masterKey;
	}

	
	
}
